import web3 from "@solana/web3.js";
import * as fs from "fs";
import bs58 from "bs58";

// STEP 1 - Connect to solona Network
const solonaConnection = new web3.Connection(
  "https://clean-maximum-log.solana-devnet.quiknode.pro/fb0a4e77b22bec1c7eb4729075a6458a9b901aa0/"
);

const keypair = web3.Keypair.generate();
console.log(`wallet public key: `, keypair.publicKey.toString());

const privateKey = bs58.encode(keypair.secretKey);
console.log(`Wallet privatekey: `, privateKey);

const serectArray = keypair.secretKey
  .toString()
  .split(",")
  .map((value) => Number(value));

const secret = JSON.stringify(serectArray);

fs.writeFile("guidSecret.json", secret, "utf8", function (err) {
  if (err) throw err;
  console.log("Wrote secret key to guideSecret.json");
});

(async () => {
  const airdropSignature = solonaConnection.requestAirdrop(
    keypair.publicKey,
    web3.LAMPORTS_PER_SOL
  );

  try {
    const txId = await airdropSignature;
    console.log(`Airdrop Transaction Id: ${txId}`);
    console.log(`https://exploer.solona.com/tx/${txId}?cluster=devnet`);
  } catch (error) {
    console.log(error);
  }
})();
