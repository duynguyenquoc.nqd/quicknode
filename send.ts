import {
  getOrCreateAssociatedTokenAccount,
  createTransferInstruction,
} from "@solana/spl-token";
import {
  Connection,
  Keypair,
  ParsedAccountData,
  PublicKey,
  sendAndConfirmTransaction,
  Transaction,
} from "@solana/web3.js";
import sourceUint8ArrayPrivateKey from "./guideSecret.json";

const SOLANA_CONNECTION = new Connection(
  "https://clean-maximum-log.solana-devnet.quiknode.pro/fb0a4e77b22bec1c7eb4729075a6458a9b901aa0/"
);

const DESTINATION_WALLET = "7UM4t1CthdJXJHaC6am6rWsZkkBLPNKvqK9dffjYBG96"; // Duy Wallet

const SOURCE_FROM_KEYPAIR = Keypair.fromSecretKey(
  new Uint8Array(sourceUint8ArrayPrivateKey)
);

const MINT_ADDRESS = "9ma7rX2mGQroh5YTFEVCiLycZSFxMXaiG9fGCDitf2FF"; // Mint Address Of Harry Coin
const TRANSFER_AMOUNT = 500;

async function getNumberDecimals(mintAddress: string): Promise<number> {
  const info = await SOLANA_CONNECTION.getParsedAccountInfo(
    new PublicKey(MINT_ADDRESS)
  );
  const result = (info.value?.data as ParsedAccountData).parsed.info
    .decimals as number;
  return result;
}

async function sendTokens() {
  console.log(`1 - Getting Source Token Account`);
  let sourceAccount = await getOrCreateAssociatedTokenAccount(
    SOLANA_CONNECTION,
    SOURCE_FROM_KEYPAIR,
    new PublicKey(MINT_ADDRESS),
    SOURCE_FROM_KEYPAIR.publicKey
  );
  console.log(`Source Account: ${sourceAccount.address.toString()}`);

  console.log(`2 - Getting Destination Token Account`);
  let destinationAccount = await getOrCreateAssociatedTokenAccount(
    SOLANA_CONNECTION,
    SOURCE_FROM_KEYPAIR,
    new PublicKey(MINT_ADDRESS),
    new PublicKey(DESTINATION_WALLET)
  );
  console.log(`Destination Account: ${destinationAccount.address.toString()}`);

  const numberDecimals = await getNumberDecimals(MINT_ADDRESS);
  const tx = new Transaction();
  tx.add(
    createTransferInstruction(
      sourceAccount.address,
      destinationAccount.address,
      SOURCE_FROM_KEYPAIR.publicKey,
      TRANSFER_AMOUNT * Math.pow(10, numberDecimals)
    )
  );

  const latestBlockHash = await SOLANA_CONNECTION.getLatestBlockhash(
    "confirmed"
  );
  tx.recentBlockhash = await latestBlockHash.blockhash;
  const signature = await sendAndConfirmTransaction(SOLANA_CONNECTION, tx, [
    SOURCE_FROM_KEYPAIR,
  ]);
  console.log(
    "\x1b[32m", //Green Text
    `   Transaction Success!🎉`,
    `\n    https://explorer.solana.com/tx/${signature}?cluster=devnet`
  );
}

sendTokens();
