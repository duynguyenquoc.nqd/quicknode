import {
  percentAmount,
  generateSigner,
  signerIdentity,
  createSignerFromKeypair,
} from "@metaplex-foundation/umi";
import {
  TokenStandard,
  createAndMint,
} from "@metaplex-foundation/mpl-token-metadata";
import { createUmi } from "@metaplex-foundation/umi-bundle-defaults";
import { mplCandyMachine } from "@metaplex-foundation/mpl-candy-machine";
import "@solana/web3.js";
import secret from "./guideSecret.json";

// Establish Solana Connection
const umi = createUmi(
  "https://clean-maximum-log.solana-devnet.quiknode.pro/fb0a4e77b22bec1c7eb4729075a6458a9b901aa0/"
); //  //Replace with your QuickNode RPC Endpoint

// Initialize the Signer wallet
// We are initializing the wallet from the secret and making it the signer for transactions.
const userWallet = umi.eddsa.createKeypairFromSecretKey(new Uint8Array(secret));
const userWalletSigner = createSignerFromKeypair(umi, userWallet);

// Creating the Metadata variable
// Create a metadata variable like below and fill in your token details. Replace IPFS_URL_OF_METADATA with the actual IPFS URL of your metadata (token.json) file.
const metadata = {
  name: "Harry Coin",
  symbol: "DC",
  uri: "https://blockchain.bighand.tech/api/health", // IPFS_URL_OF_METADATA
};

// Creating the Mint PDA
// Below, we are creating a new Mint PDA and asking the umi client to use our wallet initialized earlier from secret as a signer and use Candy Machine to mint tokens.
const mint = generateSigner(umi);
umi.use(signerIdentity(userWalletSigner));
umi.use(mplCandyMachine());

// Function to deploy Mint PDA and mint Tokens
// In the below function, we send a transaction to deploy the Mint PDA and mint 1 million of our tokens. You can change the amount of tokens and console message according to whatever suits best for you.
createAndMint(umi, {
  mint,
  authority: umi.identity,
  name: metadata.name,
  symbol: metadata.symbol,
  uri: metadata.uri,
  sellerFeeBasisPoints: percentAmount(0),
  decimals: 3,
  amount: 1000_000,
  tokenOwner: userWallet.publicKey,
  tokenStandard: TokenStandard.Fungible,
})
  .sendAndConfirm(umi)
  .then(() => {
    console.log("Successfully minted 1000 tokens (", mint.publicKey, ")");
  });
